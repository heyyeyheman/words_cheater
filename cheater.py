'''
I am a loser, so I cheat at things. This Python script is designed to aid in
my pathetic ways by helping with Words with Friends cheating. It takes about
5 minutes to process all possible strings of length 8 (the 7 letters given
by Words plus one for what's currently on the board). It prints out the
10 best scores possible, and all words that can make these scores. You'll
still have to see which words are ideal based on the circumstances of the game,
but this should do most of the hard work for you.
'''

import collections
import enchant
import itertools
import string
import sys

if __name__ == '__main__':
    if len( sys.argv ) != 2:
        print >> sys.stderr, 'usage: cheater.py letter_string'
        sys.exit()

    # initialize the data structures
    letter_string = sys.argv[ 1 ]
    dictionary = enchant.Dict( 'en_US' )
    letter_scores = {
        'a': 1,
        'b': 4,
        'c': 4,
        'd': 2,
        'e': 1,
        'f': 4,
        'g': 3,
        'h': 3,
        'i': 1,
        'j': 10,
        'k': 5,
        'l': 2,
        'm': 4,
        'n': 2,
        'o': 1,
        'p': 4,
        'q': 10,
        'r': 1,
        's': 1,
        't': 1,
        'u': 2,
        'v': 5,
        'w': 4,
        'x': 8,
        'y': 3,
        'z': 10,
    }
    valid_words = []
    word_scores = {}

    # look for all permutations of your available letters plus any one other
    # letter that produce a valid English word
    permutation_length = len( letter_string ) + 1
    while permutation_length > 0:
        print 'checking words of length {}...'.format( permutation_length ),
        for letter in string.ascii_lowercase:
            permuted_string = letter_string + letter
            permutations = list( itertools.permutations( permuted_string , permutation_length ) )

            for word in permutations:
                word = ''.join( word )
                if dictionary.check( word ) and word not in valid_words:
                    valid_words.append( word )

        permutation_length -= 1
        print 'done'

    # score the valid words
    for word in valid_words:
        score = 0
        for letter in word:
            score += letter_scores[ letter ]
        if score not in word_scores:
            word_scores[ score ] = []
        word_scores[ score ].append( word )

    # sort and show the best results
    scores = collections.OrderedDict( sorted( word_scores.items(), key=lambda t: t[ 0 ], reverse=True ) ) # this is gnarly
    best_scores = list( itertools.islice( scores.items(), 0, 10 ) ) # only show the best 10 results
    for score in best_scores:
        print score
